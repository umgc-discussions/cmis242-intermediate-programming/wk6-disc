Provide a recursive definition of some sequence of numbers or function (e.g. log, exponent, polynomial). Choose one different from that of any posted thus far. Write a recursive method that given n, computes the nth term of that sequence. Also provide an equivalent iterative implementation. How do the two implementations compare?

For my recursion program, I decided to calculate the first 20 numbers in a series of octets. This can easily be accomplished iteratively using a simple for loop where the number is multiplied by 8 for the number of times required, but can also be easily accomplished using recursion and a single method call. The recursive method is going to use more memory because it will create copies of all the variables in memory every time it has to call itself.

-Steve